﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;

namespace LR3
{
    /// <summary>
    /// Логика взаимодействия для AddAuthor.xaml
    /// </summary>
    public partial class AddAuthorWindow : Window
    {
        public Library myLibrary { get; set; }

        private MainWindow window;

        private Author author;
        public AddAuthorWindow()
        {
            InitializeComponent();
            author = new Author();
        }

        public AddAuthorWindow(Author new_author, MainWindow window)
        {
            InitializeComponent();
            author = new_author;
            myLibrary = window.myLibrary;
            this.window = window;
        }

        public AddAuthorWindow(Author new_author, AddBookWindow window)
        {
            InitializeComponent();
            author = new_author;
            myLibrary = window.myLibrary;
            this.window = window.window;
        }

        private void NewAuthorCreateClick(object sender, RoutedEventArgs e)
        {
            try
            {
                author.Name = newAuthorName.Text;
                author.DateOfBirth = (DateTime)newAuthorDateOfBirth.SelectedDate;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Недопустимый ввод");
                return;
            }
            if (!myLibrary.Authors.Contains(author, true))
            {
                myLibrary.Authors.Add(author);
                int index = window.CurrentAuthorList.SelectedIndex;
                window.SetCurrentAuthorsCollection();
                window.CurrentAuthorList.SelectedIndex = index;
                MessageBox.Show("Автор успешно добавлен!");
            }
            else
                MessageBox.Show("Данный автор уже добавлен!");
            Close();
        }

        private void GetAuthorPhoto(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            if (openDialog.ShowDialog() == true)
                author.Photo = openDialog.FileName;
        }
    }
}
