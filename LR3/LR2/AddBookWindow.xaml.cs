﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LR3
{
    /// <summary>
    /// Логика взаимодействия для AddBook.xaml
    /// </summary>
    public partial class AddBookWindow : Window
    {
        public Book book { get; set; }
        public Library myLibrary { get; set; }

        public MainWindow window { get; set; }
        public AddBookWindow()
        {
            InitializeComponent();
            book = new Book();
            myLibrary = new Library();
        }
        public AddBookWindow(MainWindow window)
        {
            InitializeComponent();
            book = new Book();
            myLibrary = new Library();
            this.window = window;
        }

        public AddBookWindow(Book new_book, Library library, MainWindow window)
        {
            InitializeComponent();
            book = new_book;
            myLibrary = library;
            newBookAuthorList.ItemsSource = myLibrary.Authors;
            newBookTagList.ItemsSource = myLibrary.Tags;
            newBookPubHouse.ItemsSource = myLibrary.PubHouse;
            this.window = window;
        }

        private void AddAuthorClick(object sender, RoutedEventArgs e)
        {
            Author new_author = new Author();
            AddAuthorWindow new_window = new AddAuthorWindow(new_author, this);
            new_window.Show();
        }

        private void AddNewTagClick(object sender, RoutedEventArgs e)
        {
            AddNewTagWindow new_window = new AddNewTagWindow(this);
            new_window.Show();
        }

        private void NewBookCreateClick(object sender, RoutedEventArgs e)
        {
            try
            {
                book.Name = newBookName.Text;
                book.ISBN = newBookISBN.Text;
                book.NumberOfPages = int.Parse(newBookNumberOfPages.Text);
                book.YearOfPublishing = int.Parse(newBookYearOfPublishing.Text);
                if (book.YearOfPublishing < 1 || book.YearOfPublishing > 2017)
                    throw new Exception();
                book.PubHouse = (PublishingHouse)newBookPubHouse.SelectedItem;
                if (!myLibrary.Books.Contains(book, true))
                {
                    foreach (Author author in newBookAuthorList.SelectedItems)
                        book.AddAuthor(myLibrary.Authors[newBookAuthorList.Items.IndexOf(author)]);
                    foreach (Tag tag in newBookTagList.SelectedItems)
                        book.AddTag(myLibrary.Tags[newBookTagList.Items.IndexOf(tag)]);
                    for (int i = 0; i < book.Authors.Count; i++)
                        book.Authors[i].AddBook(book);
                    for (int i = 0; i < book.Tags.Count; i++)
                        book.Tags[i].Books.Add(book);
                    book.PubHouse.AddBook(book);
                    myLibrary.Books.Add(book);
                    int index = window.CurrentBookList.SelectedIndex;
                    window.SetCurrentBooksCollection();
                    window.CurrentBookList.SelectedIndex = index;
                    MessageBox.Show("Книга успешно добавлена!");
                }
                else
                    MessageBox.Show("Данная книга уже добавлена!");
            }
            catch(Exception ex)
            {
                MessageBox.Show("Недопустимый ввод");
                return;
            }
            Close();
        }

        private void AddNewPubHouseClick(object sender, RoutedEventArgs e)
        {
            PublishingHouse pubhouse = new PublishingHouse();
            AddPublishingHouseWindow new_window = new AddPublishingHouseWindow(pubhouse, this);
            new_window.Show();
        }

    }
}
