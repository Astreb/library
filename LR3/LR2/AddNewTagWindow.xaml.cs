﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LR3
{
    /// <summary>
    /// Логика взаимодействия для AddNewTagWindow.xaml
    /// </summary>
    public partial class AddNewTagWindow : Window
    {
        private AddBookWindow window;
        public AddNewTagWindow()
        {
            InitializeComponent();
        }

        public AddNewTagWindow(AddBookWindow window)
        {
            InitializeComponent();
            this.window = window;
        }

        private void NewTagCreateClick(object sender, RoutedEventArgs e)
        {
            Tag tag;
            try
            {
                tag = new Tag(newTagName.Text);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Недопустимый ввод");
                return;
            }
            if (!window.myLibrary.Tags.Contains(tag))
            {
                window.myLibrary.AddTag(tag);
                MessageBox.Show("Тег успешно добавлен!");
            }
            Close();
        }
    }
}
