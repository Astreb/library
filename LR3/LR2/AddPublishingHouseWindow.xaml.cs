﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LR3
{
    /// <summary>
    /// Логика взаимодействия для AddPublisingHouseWindow.xaml
    /// </summary>
    public partial class AddPublishingHouseWindow : Window
    {
        private PublishingHouse pubhouse;
        public Library myLibrary { get; set; }
        private AddBookWindow window;
        private MainWindow mainwindow;
        public AddPublishingHouseWindow()
        {
            InitializeComponent();
            pubhouse = new PublishingHouse();
        }

        public AddPublishingHouseWindow(PublishingHouse new_pubhouse, MainWindow window)
        {
            InitializeComponent();
            pubhouse = new_pubhouse;
            myLibrary = window.myLibrary;
            mainwindow = window;
        }

        public AddPublishingHouseWindow(PublishingHouse new_pubhouse, AddBookWindow window)
        {
            InitializeComponent();
            pubhouse = new_pubhouse;
            myLibrary = window.myLibrary;
            this.window = window;
            this.mainwindow = window.window;
        }

        private void NewPubHouseCreate(object sender, RoutedEventArgs e)
        {
            try
            {
                pubhouse.Name = newPubHouseName.Text;
                pubhouse.City = newPubHouseCity.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Недопустимый ввод");
                return;
            }
            if (!myLibrary.PubHouse.Contains(pubhouse, true))
            {
                myLibrary.PubHouse.Add(pubhouse);
                int index = mainwindow.CurrentPubHouseList.SelectedIndex;
                mainwindow.SetCurrentPubHouseCollection();
                mainwindow.CurrentPubHouseList.SelectedIndex = index;
                MessageBox.Show("Издательство успешно добавлено!");
                if (window != null)
                    window.newBookPubHouse.SelectedIndex = myLibrary.PubHouse.IndexOf(pubhouse);
            }
            else
                MessageBox.Show("Данное издательство уже добавлено!");
            Close();
        }
    }
}
