﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.IO;

namespace LR3
{
    [Serializable]
    public class Author : IComparable<Author>,INotifyPropertyChanged
    {
        private string name;
        private DateTime dateOfBirth;
        private string photo;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }
        public DateTime DateOfBirth
        {
            get { return dateOfBirth; }
            set
            {
                dateOfBirth = value;
                OnPropertyChanged("DateOfBirth");
            }
        }
        public string Photo
        {
            get { return photo; }
            set
            {
                photo = value;
                OnPropertyChanged("Photo");
            }
        }
        public ObservableCollection<Book> Books;

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string par = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(par));
        }

        public int CompareTo(Author author)
        {
            if (author.Name.CompareTo(Name) != 0)
                return -1;
            if (author.DateOfBirth != DateOfBirth)
                return -1;
            return 0;
        }

        public Author()
        {
            Name = "";
            Photo = @"D:\Учёба\3сем\ИСП\LR3\LR2\bin\Debug\noPhoto.jpg";
            DateOfBirth =default(DateTime);
            Books = new ObservableCollection<Book>();
        }

        public Author(Author author)
        {
            Name = author.Name;
            Photo = author.Photo;
            DateOfBirth = author.DateOfBirth;
            Books = new ObservableCollection<Book>(author.Books);
        }

        public void SetValue(Author author)
        {
            Name = author.Name;
            Photo = author.Photo;
            DateOfBirth = author.DateOfBirth;
        }

        public void AddBook(Book book)
        {
            if (!Books.Contains(book, true))
                Books.Add(book);
        }

        public void RemoveBook(Book book)
        {
            if (Books.Contains(book, true))
                Books.RemoveAt(Books.IndexOf(book));
        }

        public void Print(StreamWriter stream)
        {
            stream.WriteLine(Name);
            stream.WriteLine(DateOfBirth);
            stream.WriteLine(Photo);
        }

        public void Read(StreamReader stream)
        {
            Name = stream.ReadLine();
            DateOfBirth = DateTime.Parse(stream.ReadLine());
            Photo = stream.ReadLine();
        }
    }
}
