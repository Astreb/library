﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.IO;

namespace LR3
{
    [Serializable]
    public class Book : IComparable<Book>, INotifyPropertyChanged
    {
        private string name;
        private string iSBN;
        private int numberOfPages;
        private int yearOfPublishing;
        private PublishingHouse pubhouse;

        public PublishingHouse PubHouse
        {
            get { return pubhouse; }
            set
            {
                pubhouse = value;
                OnPropertyChanged("PubHouse");
            }
        }
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }
        public string ISBN
        {
            get { return iSBN; }
            set
            {
                iSBN = value;
                OnPropertyChanged("ISBN");
            }
        }
        public int NumberOfPages
        {
            get { return numberOfPages; }
            set
            {
                numberOfPages = value;
                OnPropertyChanged("NumberOfPages");
            }
        }
        public int YearOfPublishing
        {
            get { return yearOfPublishing; }
            set
            {
                yearOfPublishing = value;
                OnPropertyChanged("YearOfPublishing");
            }
        }
        public ObservableCollection<Author> Authors { get; set; }
        public ObservableCollection<Tag> Tags { get; set; }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string par = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(par));
        }

        public Book()
        {
            Name = "";
            ISBN = "";
            NumberOfPages = 0;
            YearOfPublishing = 0;
            Tags = new ObservableCollection<Tag>();
            Authors = new ObservableCollection<Author>();
            PubHouse = new PublishingHouse();
        }

        public Book(Book book)
        {
            Name = book.Name;
            ISBN = book.ISBN;
            NumberOfPages = book.NumberOfPages;
            YearOfPublishing = book.YearOfPublishing;
            Tags = new ObservableCollection<Tag>(book.Tags);
            Authors = new ObservableCollection<Author>(book.Authors);
            PubHouse = new PublishingHouse(book.PubHouse);
        }

        public void SetValue(Book book)
        {
            Name = book.Name;
            ISBN = book.ISBN;
            NumberOfPages = book.NumberOfPages;
            YearOfPublishing = book.YearOfPublishing;
            Tags = new ObservableCollection<Tag>(book.Tags);
            Authors = new ObservableCollection<Author>(book.Authors);
            PubHouse = new PublishingHouse(book.PubHouse);
        }

        public int CompareTo(Book book)
        {
            if (book.Name.CompareTo(Name) != 0)
                return -1;
            if (book.ISBN.CompareTo(ISBN) != 0)
                return -1;
            if (book.NumberOfPages != NumberOfPages)
                return -1;
            if (book.YearOfPublishing != YearOfPublishing)
                return -1;
            return 0;
        }

        public void AddTag(Tag tag)
        {
            if (!Tags.Contains(tag))
                Tags.Add(tag);
        }

        public void AddAuthor(Author new_author)
        {
            if (!Authors.Contains(new_author, true))
                Authors.Add(new_author);
        }

        public void RemoveAuthor(Author author)
        {
            if (Authors.Contains(author, true))
                Authors.RemoveAt(Authors.IndexOf(author));
        }

        public void Print(StreamWriter stream)
        {
            stream.WriteLine(Name);
            stream.WriteLine(ISBN);
            stream.WriteLine(NumberOfPages);
            stream.WriteLine(YearOfPublishing);
        }

        public void Read(StreamReader stream)
        {
            Name = stream.ReadLine();
            ISBN = stream.ReadLine();
            NumberOfPages = int.Parse(stream.ReadLine());
            YearOfPublishing = int.Parse(stream.ReadLine());
        }
    }
}
