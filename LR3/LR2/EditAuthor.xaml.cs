﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace LR3
{
    /// <summary>
    /// Логика взаимодействия для EditAuthor.xaml
    /// </summary>
    public partial class EditAuthor : Window
    {
        public Author author { get; set; }
        private MainWindow window;
        public string NewPhoto { get; set; }
        public bool IsPhotoChanged { get; set; }
        public EditAuthor()
        {
            InitializeComponent();
            author = new Author();
            NewPhoto = "";
            IsPhotoChanged = false;
        }

        public EditAuthor(MainWindow window, Author author)
        {
            InitializeComponent();
            this.author = author;
            EditAuthorName.Text = author.Name;
            EditAuthorDateOfBirth.SelectedDate = author.DateOfBirth;
            NewPhoto = "";
            IsPhotoChanged = false;
            this.window = window;
        }

        private void SaveEditAuthorButtonClick(object sender, RoutedEventArgs e)
        {
            Author author_backup = new Author(author);
            try
            {
                author.Name = EditAuthorName.Text;
                author.DateOfBirth = (DateTime)EditAuthorDateOfBirth.SelectedDate;
                if (IsPhotoChanged)
                    author.Photo = NewPhoto;
                int index = window.CurrentAuthorList.SelectedIndex;
                window.SetCurrentAuthorsCollection();
                window.CurrentAuthorList.SelectedIndex = index;
                MessageBox.Show("Автор успешно изменён!");
            }
            catch(Exception ex)
            {
                MessageBox.Show("Недопустимые значения!");
                author.SetValue(author_backup);
                return;
            }
            Close();
        }

        private void GetNewAuthorPhoto(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            if (openDialog.ShowDialog() == true)
            {
                NewPhoto = openDialog.FileName;
                IsPhotoChanged = true;
            }
        }
    }
}
