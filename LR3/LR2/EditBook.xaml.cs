﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace LR3
{
    /// <summary>
    /// Логика взаимодействия для EditBook.xaml
    /// </summary>
    public partial class EditBook : Window
    {
        public Book book { get; set; }

        private MainWindow window;
        public Library myLibrary { get; set; }

        public EditBook()
        {
            InitializeComponent();
            book = new Book();
        }

        public EditBook(MainWindow window, Book book)
        {
            InitializeComponent();
            this.book = book;
            myLibrary = window.myLibrary;
            EditBookPubHouse.ItemsSource = myLibrary.PubHouse;
            EditBookPubHouse.DisplayMemberPath = "Name";
            EditBookPubHouse.SelectedIndex = myLibrary.PubHouse.IndexOf(book.PubHouse);
            EditBookAuthorList.ItemsSource = myLibrary.Authors;
            EditBookAuthorList.DisplayMemberPath = "Name";
            EditBookTagList.ItemsSource = myLibrary.Tags;
            EditBookTagList.DisplayMemberPath = "tag";
            EditBookName.Text = book.Name;
            EditBookISBN.Text = book.ISBN;
            EditBookNumberOfPages.Text = book.NumberOfPages.ToString();
            EditBookYearOfPublishing.Text = book.YearOfPublishing.ToString();
            this.window = window;
        }

        private void SaveBookButtonClick(object sender, RoutedEventArgs e)
        {
            Book book_backup = new Book(book);
            try
            {
                book.Name = EditBookName.Text;
                book.ISBN = EditBookISBN.Text;
                book.NumberOfPages = int.Parse(EditBookNumberOfPages.Text);
                book.YearOfPublishing = int.Parse(EditBookYearOfPublishing.Text);
                if (book.YearOfPublishing < 1 || book.YearOfPublishing > 2017)
                    throw new Exception();
                book.PubHouse.RemoveBook(book);
                book.PubHouse = (PublishingHouse)EditBookPubHouse.SelectedItem;
                for (int i = 0; i < book.Authors.Count; i++)
                    book.Authors[i].RemoveBook(book);
                for (int i = 0; i < book.Tags.Count; i++)
                    book.Tags[i].Books.RemoveAt(book.Tags[i].Books.IndexOf(book));
                book.Authors.Clear();
                foreach (Author author in EditBookAuthorList.SelectedItems)
                    book.Authors.Add(myLibrary.Authors[myLibrary.Authors.IndexOf(author)]);
                book.Tags.Clear();
                foreach (Tag tag in EditBookTagList.SelectedItems)
                    book.Tags.Add(myLibrary.Tags[myLibrary.Tags.IndexOf(tag)]);
                for (int i = 0; i < book.Authors.Count; i++)
                    book.Authors[i].AddBook(book);
                for (int i = 0; i < book.Tags.Count; i++)
                    book.Tags[i].Books.Add(book);
                book.PubHouse.AddBook(book);
                int index = window.CurrentBookList.SelectedIndex;
                window.SetCurrentBooksCollection();
                window.CurrentBookList.SelectedIndex = index;
                MessageBox.Show("Книга успешно изменена!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Недопустимые значения!");
                book.SetValue(book_backup);
                return;
            }
            Close();
        }
    }
}
