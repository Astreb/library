﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LR3
{
    /// <summary>
    /// Логика взаимодействия для EditPubHouse.xaml
    /// </summary>
    public partial class EditPubHouse : Window
    {
        private PublishingHouse pubhouse;
        private MainWindow window;
        public Library myLibrary { get; set; }
        public EditPubHouse()
        {
            InitializeComponent();
            pubhouse = new PublishingHouse();
        }

        public EditPubHouse(MainWindow window, PublishingHouse pubhouse)
        {
            InitializeComponent();
            this.pubhouse = pubhouse;
            EditPubHouseName.Text = pubhouse.Name;
            EditPubHouseCity.Text = pubhouse.City;
            this.window = window;
        }

        private void SaveEditPubHouseButtonClick(object sender, RoutedEventArgs e)
        {
            PublishingHouse pubhouse_backup = new PublishingHouse(pubhouse);
            try
            {
                pubhouse.Name = EditPubHouseName.Text;
                pubhouse.City = EditPubHouseCity.Text;
                int index = window.CurrentPubHouseList.SelectedIndex;
                window.SetCurrentPubHouseCollection();
                window.CurrentPubHouseList.SelectedIndex = index;
                MessageBox.Show("Издательство успешно изменено!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Недопустимые значения!");
                pubhouse.SetValue(pubhouse_backup);
                return;
            }
            Close();
        }
    }
}
