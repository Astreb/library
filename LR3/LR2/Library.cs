﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.IO;

namespace LR3
{
    [Serializable]
    public class Library
    {
        public ObservableCollection<Book> Books;
        public ObservableCollection<Author> Authors;
        public ObservableCollection<PublishingHouse> PubHouse;
        public ObservableCollection<Tag> Tags;

        public void AddPubHouse(MainWindow window)
        {
            PublishingHouse PHouse = new PublishingHouse();
            AddPublishingHouseWindow new_window = new AddPublishingHouseWindow(PHouse, window);
            new_window.Show();
        }

        public void AddAuthor(MainWindow window)
        {
            Author author = new Author();
            AddAuthorWindow new_window = new AddAuthorWindow(author, window);
            new_window.Show();
        }

        public void AddBook(MainWindow window)
        {
            Book new_book = new Book();
            AddBookWindow new_window = new AddBookWindow(new_book, this, window);
            new_window.myLibrary = window.myLibrary;
            new_window.Show();
        }

        public bool AddTag(Tag tag)
        {
            if (Tags.Contains(tag))
                return false;
            Tags.Add(tag);
            return true;
        }

        public void DeletePubHouse(int index)
        {
            for (int i = 0; i < PubHouse[index].Books.Count; i++)
            {
                Books[Books.IndexOf(PubHouse[index].Books[i])].PubHouse = new PublishingHouse();
                PubHouse[index].Books[i].PubHouse = new PublishingHouse();
            }
            PubHouse[index].Books.Clear();
            PubHouse.RemoveAt(index);
        }

        public void DeleteAuthor(int index)
        {
            for (int i = 0; i < Authors[index].Books.Count; i++)
                Authors[index].Books[i].RemoveAuthor(Authors[index]);
            Authors[index].Books.Clear();
            Authors.RemoveAt(index);
        }

        public void DeleteBook(int index)
        {
            for (int i = 0; i < Books[index].Authors.Count; i++)
                Books[index].Authors[i].RemoveBook(Books[index]);
            Books[index].Authors.Clear();
            Books[index].PubHouse.RemoveBook(Books[index]);
            Books[index].PubHouse = null;
            Books.RemoveAt(index);
        }

        public Library()
        {
            Books = new ObservableCollection<Book>();
            Authors = new ObservableCollection<Author>();
            PubHouse = new ObservableCollection<PublishingHouse>();
            Tags = new ObservableCollection<Tag>();
        }
        public Library(Library library)
        {
            this.Books = new ObservableCollection<Book>(library.Books);
            this.Authors = new ObservableCollection<Author>(library.Authors);
            this.PubHouse = new ObservableCollection<PublishingHouse>(library.PubHouse);
            this.Tags = new ObservableCollection<Tag>(library.Tags);
        }

        #region SaveToTextFile
        public void PrintBooks(StreamWriter stream)
        {
            try
            {
                foreach (Book book in Books)
                {
                    stream.WriteLine(book.Name);
                    stream.WriteLine(book.ISBN);
                    stream.WriteLine(book.NumberOfPages);
                    stream.WriteLine(book.YearOfPublishing);
                    book.PubHouse.Print(stream);
                    stream.WriteLine(book.Authors.Count);
                    foreach (Author author in book.Authors)
                        author.Print(stream);
                    stream.WriteLine(book.Tags.Count);
                    foreach (Tag tag in book.Tags)
                        stream.WriteLine(tag.tag);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при записи данных в файл");
                return;
            }
        }

        public void PrintAuthors(StreamWriter stream)
        {
            try
            {
                foreach (Author author in Authors)
                {
                    stream.WriteLine(author.Name);
                    stream.WriteLine(author.DateOfBirth);
                    stream.WriteLine(author.Photo);
                    stream.WriteLine(author.Books.Count);
                    foreach (Book book in author.Books)
                        book.Print(stream);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при записи данных в файл");
                return;
            }
        }

        public void PrintPubHouse(StreamWriter stream)
        {
            try
            {
                foreach (PublishingHouse pb in PubHouse)
                {
                    stream.WriteLine(pb.Name);
                    stream.WriteLine(pb.City);
                    stream.WriteLine(pb.Books.Count);
                    foreach (Book book in pb.Books)
                        book.Print(stream);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при записи данных в файл");
                return;
            }
        }

        public void PrintTags(StreamWriter stream)
        {
            try
            {
                foreach (Tag tag in Tags)
                {
                    stream.WriteLine(tag.tag);
                    stream.WriteLine(tag.Books.Count);
                    foreach (Book book in tag.Books)
                        book.Print(stream);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при записи данных в файл");
                return;
            }
        }

        #endregion

        #region LoadFromTextFile
        public void ReadBooks(StreamReader stream, int booksCount)
        {
            try
            {
                for (int i = 0; i < booksCount; i++)
                {
                    Book book = new Book();
                    book.Name = stream.ReadLine();
                    book.ISBN = stream.ReadLine();
                    book.NumberOfPages = int.Parse(stream.ReadLine());
                    book.YearOfPublishing = int.Parse(stream.ReadLine());
                    book.PubHouse.Read(stream);
                    int authorsCount = int.Parse(stream.ReadLine());
                    for (int j = 0; j < authorsCount; j++)
                    {
                        Author author = new Author();
                        author.Read(stream);
                        if (!Authors.Contains(author, true))
                            Authors.Add(author);
                    }
                    int tagsCount = int.Parse(stream.ReadLine());
                    for (int j = 0; j < tagsCount; j++)
                    {
                        Tag tag = new Tag();
                        tag.tag = stream.ReadLine();
                        if (!Tags.Contains(tag, true))
                            Tags.Add(tag);
                    }
                    Books.Add(book);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при чтении данных из файла");
                return;
            }
        }

        public void ReadAuthors(StreamReader stream, int authorsCount)
        {
            try
            {
                for (int i = 0; i < authorsCount; i++)
                {
                    Author author = new Author();
                    author.Name = stream.ReadLine();
                    author.DateOfBirth = DateTime.Parse(stream.ReadLine());
                    author.Photo = stream.ReadLine();
                    if (Authors.Contains(author, true))
                        author = Authors[Authors.GetElementIndex(author)];
                    int booksCount = int.Parse(stream.ReadLine());
                    for (int j = 0; j < booksCount; j++)
                    {
                        Book book = new Book();
                        book.Read(stream);
                        author.AddBook(Books[Books.GetElementIndex(book)]);
                        Books[Books.GetElementIndex(book)].AddAuthor(author);
                    }
                    if (!Authors.Contains(author, true))
                        Authors.Add(author);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при чтении данных из файла");
                return;
            }
        }

        public void ReadPubHouse(StreamReader stream, int pubhouseCount)
        {
            try
            {
                for (int i = 0; i < pubhouseCount; i++)
                {
                    PublishingHouse pb = new PublishingHouse();
                    pb.Name = stream.ReadLine();
                    pb.City = stream.ReadLine();
                    int booksCount = int.Parse(stream.ReadLine());
                    for (int j = 0; j < booksCount; j++)
                    {
                        Book book = new Book();
                        book.Read(stream);
                        pb.AddBook(Books[Books.GetElementIndex(book)]);
                    }
                    PubHouse.Add(pb);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при чтении данных из файла");
                return;
            }
        }

        public void ReadTags(StreamReader stream, int tagsCount)
        {
            try
            {
                for (int i = 0; i < tagsCount; i++)
                {
                    Tag tag = new Tag();
                    tag.tag = stream.ReadLine();
                    if (Tags.Contains(tag, true))
                        tag = Tags[Tags.GetElementIndex(tag)];
                    int booksCount = int.Parse(stream.ReadLine());
                    for (int j = 0; j < booksCount; j++)
                    {
                        Book book = new Book();
                        book.Read(stream);
                        tag.Books.Add(Books[Books.GetElementIndex(book)]);
                        Books[Books.GetElementIndex(book)].AddTag(tag);
                    }
                    if (!Tags.Contains(tag, true))
                        Tags.Add(tag);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при чтении данных из файла");
                return;
            }
        }

        #endregion
    }
}
