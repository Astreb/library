﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO.Compression;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using MyInterface;
using System.Reflection;
using System.Threading;
using System.Configuration;

namespace LR3
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Library myLibrary { get; set; }

        public ObservableCollection<Book> CurrentBooksCollection { get; set; }
        public ObservableCollection<Author> CurrentAuthorsCollection { get; set; }
        public ObservableCollection<PublishingHouse> CurrentPubHouseCollection { get; set; }

        private List<IPluggin> pluggins = new List<IPluggin>();
        private string PlugginsPath = "";

        public MainWindow()
        {
            InitializeComponent();
            Start();
        }

        public void Start()
        {
            myLibrary = new Library();
            SetComponentBinding();
            this.Show();
            SetCommand();
            LoadPlugginsAsync();
        }

        public void SetComponentBinding()
        {
            CurrentBooksCollection = new ObservableCollection<Book>();
            CurrentAuthorsCollection = new ObservableCollection<Author>();
            CurrentPubHouseCollection = new ObservableCollection<PublishingHouse>();
            CurrentBooksCollection.SetCollection(myLibrary.Books);
            CurrentAuthorsCollection.SetCollection(myLibrary.Authors);
            CurrentPubHouseCollection.SetCollection(myLibrary.PubHouse);
            CurrentBookList.ItemsSource = CurrentBooksCollection;
            //CurrentBookList.DisplayMemberPath = "Name";
            CurrentAuthorList.ItemsSource = CurrentAuthorsCollection;
            //CurrentAuthorList.DisplayMemberPath = "Name";
            CurrentPubHouseList.ItemsSource = CurrentPubHouseCollection;
            //CurrentPubHouseList.DisplayMemberPath = "Name";
            booksFindSelector.SelectedIndex = 0;
            authorsFindSelector.SelectedIndex = 0;
            publisingFindSelector.SelectedIndex = 0;
        }
        private void CollectionsSizeChange(object sender, SizeChangedEventArgs e)
        {
            publishinghouses.Width = Width / 3.0;
            authors.Width = publishinghouses.Width;
            books.Width = authors.Width - 20;
        }

        private void WindowStateChanged(object sender, EventArgs e)
        {
            publishinghouses.Width = Width / 3.0;
            authors.Width = publishinghouses.Width;
            books.Width = authors.Width - 20;
        }

        #region AddElements
        private void AddPublishingHouseClick(object sender, RoutedEventArgs e)
        {
            myLibrary.AddPubHouse(this);
        }

        private void AddAuthorClick(object sender, RoutedEventArgs e)
        {
            myLibrary.AddAuthor(this);
        }

        private void AddBookClick(object sender, RoutedEventArgs e)
        { 
            myLibrary.AddBook(this);
        }

        #endregion

        #region DeleteElements

        private void DeleteBookClick(object sender, RoutedEventArgs e)
        {
            int index = CurrentBookList.SelectedIndex;
            if (index != -1)
            {
                index = myLibrary.Books.IndexOf((Book)CurrentBookList.SelectedItem);
                CurrentBookList.SelectedIndex = -1;
                myLibrary.DeleteBook(index);
                SetCurrentBooksCollection();
                if (index >= CurrentBookList.Items.Count)
                    index = CurrentBookList.Items.Count - 1;
                CurrentBookList.SelectedIndex = index;
                MessageBox.Show("Книга успешно удалена!");
            }
            else
                MessageBox.Show("Выберите книгу!");
        }

        private void DeletePubHouseClick(object sender, RoutedEventArgs e)
        {
            int index = CurrentPubHouseList.SelectedIndex;
            if (index != -1)
            {
                index = myLibrary.PubHouse.IndexOf((PublishingHouse)CurrentPubHouseList.SelectedItem);
                CurrentPubHouseList.SelectedIndex = -1;
                myLibrary.DeletePubHouse(index);
                SetCurrentPubHouseCollection();
                if (index >= CurrentPubHouseList.Items.Count)
                    index = CurrentPubHouseList.Items.Count - 1;
                CurrentPubHouseList.SelectedIndex = index;
                MessageBox.Show("Издательство успешно удалено!");
            }
            else
                MessageBox.Show("Выберите издательство!");
        }

        private void DeleteAuthorClick(object sender, RoutedEventArgs e)
        {
            int index = CurrentAuthorList.SelectedIndex;
            if (index != -1)
            {
                index = myLibrary.Authors.IndexOf((Author)CurrentAuthorList.SelectedItem);
                CurrentAuthorList.SelectedIndex = -1;
                myLibrary.DeleteAuthor(index);
                SetCurrentAuthorsCollection();
                if (index >= CurrentAuthorList.Items.Count)
                    index = CurrentAuthorList.Items.Count - 1;
                CurrentAuthorList.SelectedIndex = index;
                MessageBox.Show("Автор успешно удалён!");
            }
            else
                MessageBox.Show("Выберите автора!");
        }

        #endregion

        #region Changed

        private void FirstBook(object sender, RoutedEventArgs e)
        {
            if (CurrentBookList.Items.Count > 0)
                CurrentBookList.SelectedIndex = 0;
            else
                CurrentBookList.SelectedIndex = -1;
        }

        private void LastBook(object sender, RoutedEventArgs e)
        {
            if (CurrentBookList.Items.Count > 0)
                CurrentBookList.SelectedIndex = CurrentBookList.Items.Count - 1;
            else
                CurrentBookList.SelectedIndex = -1;
        }

        private void FirstAuthor(object sender, RoutedEventArgs e)
        {
            if (CurrentAuthorList.Items.Count > 0)
                CurrentAuthorList.SelectedIndex = 0;
            else
                CurrentAuthorList.SelectedIndex = -1;
        }

        private void LastAuthor(object sender, RoutedEventArgs e)
        {
            if (CurrentAuthorList.Items.Count > 0)
                CurrentAuthorList.SelectedIndex = CurrentAuthorList.Items.Count - 1;
            else
                CurrentAuthorList.SelectedIndex = -1;
        }

        private void FirstPubHouse(object sender, RoutedEventArgs e)
        {
            if (CurrentPubHouseList.Items.Count > 0)
                CurrentPubHouseList.SelectedIndex = 0;
            else
                CurrentPubHouseList.SelectedIndex = -1;
        }

        private void LastPubHouse(object sender, RoutedEventArgs e)
        {
            if (CurrentPubHouseList.Items.Count > 0)
                CurrentPubHouseList.SelectedIndex = CurrentPubHouseList.Items.Count - 1;
            else
                CurrentPubHouseList.SelectedIndex = -1;
        }

        private void CurrentBookListSelectionChanged(object sender, RoutedEventArgs e)
        {
            if (CurrentBookList.SelectedIndex != -1)
            {
                bookAuthorsList.ItemsSource = ((Book)CurrentBookList.SelectedItem).Authors;
                //bookAuthorsList.DisplayMemberPath = "Name";
                bookTagList.ItemsSource = ((Book)CurrentBookList.SelectedItem).Tags;
                //bookTagList.DisplayMemberPath = "tag";
            }
            else
            {
                bookAuthorsList.ItemsSource = new ObservableCollection<Author>();
                bookTagList.ItemsSource = new ObservableCollection<Tag>();
            }
        }

        private void NextBook(object sender, RoutedEventArgs e)
        {
            int index = CurrentBookList.SelectedIndex + 1;
            if (index >= CurrentBookList.Items.Count)
            {
                MessageBox.Show("Эта книга является последней!");
                return;
            }
            if (CurrentBookList.Items.Count == 0)
            {
                MessageBox.Show("Список пуст!");
                return;
            }
            CurrentBookList.SelectedIndex = index;
        }

        private void PrefBook(object sender, RoutedEventArgs e)
        {
            int index = CurrentBookList.SelectedIndex - 1;
            if (index < 0)
            {
                MessageBox.Show("Эта книга является первой!");
                return;
            }
            if (CurrentBookList.Items.Count == 0)
            {
                MessageBox.Show("Список пуст!");
                return;
            }
            CurrentBookList.SelectedIndex = index;
        }

        private void CurrentAuthorListSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CurrentAuthorList.SelectedIndex != -1)
            {
                authorBookList.ItemsSource = ((Author)CurrentAuthorList.SelectedItem).Books;
                //authorBookList.DisplayMemberPath = "Name";
            }
            else
            {
                authorBookList.ItemsSource = new ObservableCollection<Book>();
                AuthorPhoto = null;
            }
        }

        private void NextAuthor(object sender, RoutedEventArgs e)
        {
            int index = CurrentAuthorList.SelectedIndex + 1;
            if (index >= CurrentAuthorList.Items.Count)
            {
                MessageBox.Show("Этот автор является последним!");
                return;
            }
            if (CurrentAuthorList.Items.Count == 0)
            {
                MessageBox.Show("Список пуст!");
                return;
            }
            CurrentAuthorList.SelectedIndex = index;
        }

        private void PrefAuthor(object sender, RoutedEventArgs e)
        {
            int index = CurrentAuthorList.SelectedIndex - 1;
            if (index < 0)
            {
                MessageBox.Show("Этот автор является первым!");
                return;
            }
            if (CurrentAuthorList.Items.Count == 0)
            {
                MessageBox.Show("Список пуст!");
                return;
            }
            CurrentAuthorList.SelectedIndex = index;
        }

        private void CurrentPubHouseListSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CurrentPubHouseList.SelectedIndex != -1)
            {
                PubHouseBookList.ItemsSource = ((PublishingHouse)CurrentPubHouseList.SelectedItem).Books;
                //PubHouseBookList.DisplayMemberPath = "Name";
            }
            else
                PubHouseBookList.ItemsSource = new ObservableCollection<Book>();
        }

        private void NextPubHouse(object sender, RoutedEventArgs e)
        {
            int index = CurrentPubHouseList.SelectedIndex + 1;
            if (index >= CurrentPubHouseList.Items.Count)
            {
                MessageBox.Show("Это издательство является последним!");
                return;
            }
            if (CurrentPubHouseList.Items.Count == 0)
            {
                MessageBox.Show("Список пуст!");
                return;
            }
            CurrentPubHouseList.SelectedIndex = index;
        }

        private void PrefPubHouse(object sender, RoutedEventArgs e)
        {
            int index = CurrentPubHouseList.SelectedIndex - 1;
            if (index < 0)
            {
                MessageBox.Show("Это издательство является первым!");
                return;
            }
            if (CurrentPubHouseList.Items.Count == 0)
            {
                MessageBox.Show("Список пуст!");
                return;
            }
            CurrentPubHouseList.SelectedIndex = index;
        }

        #endregion

        #region Edit

        private void EditBookButtonClick(object sender, RoutedEventArgs e)
        {
            if (CurrentBookList.SelectedIndex == -1)
            {
                MessageBox.Show("Выберите книгу!");
                return;
            }
            EditBook window = new EditBook(this, myLibrary.Books[myLibrary.Books.IndexOf((Book)CurrentBookList.SelectedItem)]);
            window.Show();
        }

        private void EditAuthorButtonClick(object sender, RoutedEventArgs e)
        {
            if (CurrentAuthorList.SelectedIndex == -1)
            {
                MessageBox.Show("Выберите автора!");
                return;
            }
            EditAuthor window = new EditAuthor(this, myLibrary.Authors[myLibrary.Authors.IndexOf((Author)CurrentAuthorList.SelectedItem)]);
            window.Show();
        }

        private void EditPubHouseButtonClick(object sender, RoutedEventArgs e)
        {
            if (CurrentPubHouseList.SelectedIndex == -1)
            {
                MessageBox.Show("Выберите издательство!");
                return;
            }
            EditPubHouse window = new EditPubHouse(this, myLibrary.PubHouse[myLibrary.PubHouse.IndexOf((PublishingHouse)CurrentPubHouseList.SelectedItem)]);
            window.Show();
        }

        #endregion

        #region SerealizeAndDeserealize
        private void LibrarySerialize(string filename)
        {
            BinaryFormatter formater = new BinaryFormatter();
            using (FileStream fs = new FileStream(filename, FileMode.OpenOrCreate))
            {
                formater.Serialize(fs, myLibrary);
            }
        }

        private void LibraryDeserealize(string filename)
        {
            BinaryFormatter formater = new BinaryFormatter();
            using (FileStream fs = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Read))
            {
                Library lib = (Library)formater.Deserialize(fs);
                if (lib.Books != null)
                {
                    foreach (Book i in lib.Books)
                        if (!myLibrary.Books.Contains(i, true))
                            myLibrary.Books.Add(i);
                }
                if (lib.Authors != null)
                {
                    foreach (Author i in lib.Authors)
                        if (!myLibrary.Authors.Contains(i, true))
                            myLibrary.Authors.Add(i);
                }
                if (lib.PubHouse != null)
                {
                    foreach (PublishingHouse i in lib.PubHouse)
                        if (!myLibrary.PubHouse.Contains(i, true))
                            myLibrary.PubHouse.Add(i);
                }
                if (lib.Tags != null)
                {
                    foreach (Tag i in lib.Tags)
                        if (!myLibrary.Tags.Contains(i))
                            myLibrary.Tags.Add(i);
                }
            }
            SetCurrentCollections();
        }

        #endregion

        #region CompressionAndDecompression
        private void CompressionData(object sender, RoutedEventArgs e)
        {
            try
            {
                BinaryFormatter formater = new BinaryFormatter();
                using (FileStream fs = new FileStream("temp.dat", FileMode.OpenOrCreate))
                {
                    formater.Serialize(fs, myLibrary);
                }
                using (FileStream sourceStream = new FileStream("temp.dat", FileMode.OpenOrCreate))
                {
                    SaveFileDialog saveDialog = new SaveFileDialog();
                    if (saveDialog.ShowDialog() == true)
                    {
                        LibrarySerialize(saveDialog.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Не удалось заархивировать данные");
                return;
            }
        }

        private void DecompressionData(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog openDialog = new OpenFileDialog();
                if (openDialog.ShowDialog() == true)
                {
                    using (FileStream sourceStream = new FileStream(openDialog.FileName, FileMode.OpenOrCreate))
                    {
                        using (FileStream targetStream = new FileStream("temp.dat", FileMode.OpenOrCreate))
                        {
                            using (GZipStream decompressionStream = new GZipStream(sourceStream, CompressionMode.Decompress))
                            {
                                decompressionStream.CopyTo(targetStream);
                            }
                        }
                    }
                    LibraryDeserealize(openDialog.FileName);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Не удалось разархивироват данные");
                return;
            }
        }

        #endregion

        #region File

        private void OpenTextFile(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog openDialog = new OpenFileDialog();
                if (openDialog.ShowDialog() == true)
                {
                    using (StreamReader stream = new StreamReader(openDialog.FileName))
                    {
                        Library lib = new Library();
                        int booksCount = int.Parse(stream.ReadLine());
                        lib.ReadBooks(stream, booksCount);
                        int authorsCount = int.Parse(stream.ReadLine());
                        lib.ReadAuthors(stream, authorsCount);
                        int pubhouseCount = int.Parse(stream.ReadLine());
                        lib.ReadPubHouse(stream, pubhouseCount);
                        int tagsCount = int.Parse(stream.ReadLine());
                        lib.ReadTags(stream, tagsCount);
                        if (lib.Books != null)
                        {
                            foreach (Book i in lib.Books)
                                if (!myLibrary.Books.Contains(i, true))
                                    myLibrary.Books.Add(i);
                        }
                        if (lib.Authors != null)
                        {
                            foreach (Author i in lib.Authors)
                                if (!myLibrary.Authors.Contains(i, true))
                                    myLibrary.Authors.Add(i);
                        }
                        if (lib.PubHouse != null)
                        {
                            foreach (PublishingHouse i in lib.PubHouse)
                                if (!myLibrary.PubHouse.Contains(i, true))
                                    myLibrary.PubHouse.Add(i);
                        }
                        if (lib.Tags != null)
                        {
                            foreach (Tag i in lib.Tags)
                                if (!myLibrary.Tags.Contains(i))
                                    myLibrary.Tags.Add(i);
                        }
                        SetCurrentCollections();
                    }
                    MessageBox.Show("Загружено", "", MessageBoxButton.OK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при чтении данных из файла");
                return;
            }
        }
        private void OpenBinaryFile(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog openDialog = new OpenFileDialog();
                if (openDialog.ShowDialog() == true)
                {
                    LibraryDeserealize(openDialog.FileName);
                    MessageBox.Show("Загружено", "", MessageBoxButton.OK);
                    SetCurrentCollections();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Не удалось открыть файл");
                return;
            }
        }

        private void SaveTextFile(object sender, RoutedEventArgs e)
        {
            try
            {
                SaveFileDialog saveDialog = new SaveFileDialog();
                if (saveDialog.ShowDialog() == true)
                {
                    using (StreamWriter stream = new StreamWriter(saveDialog.FileName))
                    {
                        stream.WriteLine(myLibrary.Books.Count);
                        myLibrary.PrintBooks(stream);
                        stream.WriteLine(myLibrary.Authors.Count);
                        myLibrary.PrintAuthors(stream);
                        stream.WriteLine(myLibrary.PubHouse.Count);
                        myLibrary.PrintPubHouse(stream);
                        stream.WriteLine(myLibrary.Tags.Count);
                        myLibrary.PrintTags(stream);
                    }
                    MessageBox.Show("Сохранено", "", MessageBoxButton.OK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при записи данных в файл");
                return;
            }
        }

        private void SaveBinaryFile(object sender, RoutedEventArgs e)
        {
            try
            {
                SaveFileDialog saveDialog = new SaveFileDialog();
                if (saveDialog.ShowDialog() == true)
                {
                    LibrarySerialize(saveDialog.FileName);
                    MessageBox.Show("Сохранено", "", MessageBoxButton.OK);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Не удалось сохранить в указанный файл");
                return;
            }
        }

        #endregion

        #region Find

        public void SetCurrentCollections()
        {
            SetCurrentBooksCollection();
            SetCurrentAuthorsCollection();
            SetCurrentPubHouseCollection();
        }

        public void SetCurrentBooksCollection()
        {
            string par = booksFindTextBox.Text;
            int index = booksFindSelector.SelectedIndex;
            switch (index)
            {
                case 0:
                    {
                        CurrentBooksCollection.SetCollection(myLibrary.Books);
                        booksFindTextBox.Text = "";
                        break;
                    }
                case 1:
                    {
                        CurrentBooksCollection.SetCollection(new ObservableCollection<Book>(myLibrary.Books.Where<Book>(i => i.Name == par).OrderBy(book => book.Name)));
                        break;
                    }
                case 2:
                    {
                        CurrentBooksCollection.SetCollection(new ObservableCollection<Book>(myLibrary.Books.Where(book => book.PubHouse.Name == par).OrderBy(book => book.Name)));
                        break;
                    }
                case 3:
                    {
                        CurrentBooksCollection.SetCollection(new ObservableCollection<Book>(myLibrary.Books.Where(book => book.Tags.Any(tag => tag.tag == par)).OrderBy(book => book.Name)));
                        break;
                    }
                case 4:
                    {
                        CurrentBooksCollection.SetCollection(new ObservableCollection<Book>(myLibrary.Books.Where(book => book.Authors.Any(author => author.Name == par)).OrderBy(book => book.Name)));
                        break;
                    }
            }
        }

        public void SetCurrentAuthorsCollection()
        {
            string par = authorsFindTextBox.Text;
            int index = authorsFindSelector.SelectedIndex;
            switch (index)
            {
                case 0:
                    {
                        CurrentAuthorsCollection.SetCollection(myLibrary.Authors);
                        authorsFindTextBox.Text = "";
                        break;
                    }
                case 1:
                    {
                        CurrentAuthorsCollection.SetCollection(new ObservableCollection<Author>(myLibrary.Authors.AsParallel().Where(author => author.Name == par).OrderBy(author => author.Name)));
                        break;
                    }
                case 2:
                    {
                        CurrentAuthorsCollection.SetCollection(new ObservableCollection<Author>(myLibrary.Authors.AsParallel().Where(author => author.DateOfBirth == DateTime.Parse(par)).OrderBy(author => author.Name)));
                        break;
                    }
            }

        }

        public void SetCurrentPubHouseCollection()
        {
            string par = publishingFindTextBox.Text;
            int index = publisingFindSelector.SelectedIndex;
            switch (index)
            {
                case 0:
                    {
                        CurrentPubHouseCollection.SetCollection(myLibrary.PubHouse);
                        publishingFindTextBox.Text = "";
                        break;
                    }
                case 1:
                    {
                        CurrentPubHouseCollection.SetCollection(myLibrary.PubHouse.GroupBy(pubhouse => pubhouse.Name).Where(group => group.Key == par));
                        break;
                    }
                case 2:
                    {
                        CurrentPubHouseCollection.SetCollection(myLibrary.PubHouse.GroupBy(pubhouse => pubhouse.City).Where(group => group.Key == par));
                        break;
                    }
            }
        }
        private void AuthorsFindButtonClick(object sender, RoutedEventArgs e)
        {
            SetCurrentAuthorsCollection();
            MessageBox.Show($"Поиск завершён. \nВсего найдено: {CurrentAuthorsCollection.Count()}");
        }

        private void BooksFindButtonClick(object sender, RoutedEventArgs e)
        {
            SetCurrentBooksCollection();
            MessageBox.Show($"Поиск завершён. \nВсего найдено: {CurrentBooksCollection.Count()}");
        }

        private void PublishingFindButtonClick(object sender, RoutedEventArgs e)
        {
            SetCurrentPubHouseCollection();
            MessageBox.Show($"Поиск завершён. \nВсего найдено: {CurrentPubHouseCollection.Count()}");
        }
        #endregion

        #region Pluggins

        private void PlugginsClick(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            foreach (IPluggin pluggin in pluggins)
                if ((string)btn.Content == pluggin.Name)
                {
                    pluggin.Process(myLibrary);
                    SetCurrentCollections();
                    MessageBox.Show("Выполнено", "", MessageBoxButton.OK);
                }
            plug.IsSubmenuOpen = false;
        }

        private async void LoadPlugginsAsync()
        {
            if (PlugginsPath == "")
                return;
            string path = PlugginsPath;
            string[] files = Directory.GetFiles(path, "*.dll");

            pluggins.Clear();
            foreach (string filename in files)
            {
                Assembly assembly = Assembly.LoadFrom(filename);
                foreach (Type tp in assembly.GetExportedTypes())
                    if (tp.IsClass && typeof(IPluggin).IsAssignableFrom(tp))
                        pluggins.Add((IPluggin)Activator.CreateInstance(tp));
            }

            Task<Button>[] buttons = new Task<Button>[pluggins.Count];
            for (int i = 0; i < pluggins.Count; i++)
            {
                Button btn = new Button();
                btn.Content = pluggins[i].Name;
                btn.Style = null;
                buttons[i] = Task.Factory.StartNew<Button>(()=>
                {
                    btn.Click += new RoutedEventHandler(PlugginsClick);
                    return btn;
                });
                buttons[i].Wait();
                plug.Items.Add(btn);
            }
            await Task.WhenAll<Button>(buttons);
            await Task.Factory.StartNew(()=>Thread.Sleep(2000));
            plug.Visibility = Visibility.Visible;
        }

        #endregion

        #region HotKeys
        public void SetCommand()
        {
            KeyGesture hotKey;
            string stringkey = null;
            if (ConfigurationManager.AppSettings["PlugginsPath"] != null)
            {
                stringkey = ConfigurationManager.AppSettings["PlugginsPath"];
                PlugginsPath = stringkey;
            }
            if (ConfigurationManager.AppSettings["Загрузить из текстового файла"] != null)
            {
                stringkey = ConfigurationManager.AppSettings["Загрузить из текстового файла"];
                hotKey = GetKey(stringkey);
                if (hotKey != null)
                {
                    RoutedCommand command = new RoutedCommand("OpenTextFile", typeof(MainWindow));
                    command.Execute(LoadFromTextFile, null);
                    LoadFromTextFile.InputGestureText = stringkey;
                    InputBindings.Add(new InputBinding(command, hotKey));
                    CommandBindings.Add(new CommandBinding(command, OpenTextFile));
                }
            }

            if (ConfigurationManager.AppSettings["Загрузить из бинарного файла"] != null)
            {
                stringkey = ConfigurationManager.AppSettings["Загрузить из бинарного файла"];
                hotKey = GetKey(stringkey);
                if (hotKey != null)
                {
                    RoutedCommand command = new RoutedCommand("OpenBinaryFile", typeof(MainWindow));
                    command.Execute(LoadFromBinaryFile, null);
                    LoadFromBinaryFile.InputGestureText = stringkey;
                    InputBindings.Add(new InputBinding(command, hotKey));
                    CommandBindings.Add(new CommandBinding(command, OpenBinaryFile));
                }
            }

            if (ConfigurationManager.AppSettings["Сохранить в текстовый файл"] != null)
            {
                stringkey = ConfigurationManager.AppSettings["Сохранить в текстовый файл"];
                hotKey = GetKey(stringkey);
                if (hotKey != null)
                {
                    RoutedCommand command = new RoutedCommand("SaveToTextFile", typeof(MainWindow));
                    command.Execute(SaveToTextFile, null);
                    SaveToTextFile.InputGestureText = stringkey;
                    InputBindings.Add(new InputBinding(command, hotKey));
                    CommandBindings.Add(new CommandBinding(command, SaveTextFile));
                }
            }

            if (ConfigurationManager.AppSettings["Сохранить в бинарный файл"] != null)
            {
                stringkey = ConfigurationManager.AppSettings["Сохранить в бинарный файл"];
                hotKey = GetKey(stringkey);
                if (hotKey != null)
                {
                    RoutedCommand command = new RoutedCommand("SaveToBinaryFile", typeof(MainWindow));
                    command.Execute(SaveToBinaryFile, null);
                    SaveToBinaryFile.InputGestureText = stringkey;
                    InputBindings.Add(new InputBinding(command, hotKey));
                    CommandBindings.Add(new CommandBinding(command, SaveBinaryFile));
                }
            }

            if (ConfigurationManager.AppSettings["Заархивировать данные"] != null)
            {
                stringkey = ConfigurationManager.AppSettings["Заархивировать данные"];
                hotKey = GetKey(stringkey);
                if (hotKey != null)
                {
                    RoutedCommand command = new RoutedCommand("CompData", typeof(MainWindow));
                    command.Execute(CompData, null);
                    CompData.InputGestureText = stringkey;
                    InputBindings.Add(new InputBinding(command, hotKey));
                    CommandBindings.Add(new CommandBinding(command, CompressionData));
                }
            }

            if (ConfigurationManager.AppSettings["Извлечь данные из архива"] != null)
            {
                stringkey = ConfigurationManager.AppSettings["Извлечь данные из архива"];
                hotKey = GetKey(stringkey);
                if (hotKey != null)
                {
                    RoutedCommand command = new RoutedCommand("DecompData", typeof(MainWindow));
                    command.Execute(DecompData, null);
                    DecompData.InputGestureText = stringkey;
                    InputBindings.Add(new InputBinding(command, hotKey));
                    CommandBindings.Add(new CommandBinding(command, DecompressionData));
                }
            }
        }

        public KeyGesture GetKey(string stringkey)
        {
            if (stringkey == null || stringkey == "")
                return null;
            KeyGestureConverter converter = new KeyGestureConverter();
            try
            {
                KeyGesture gesture = (KeyGesture)converter.ConvertFromString(stringkey);
                return gesture;
            }
            catch (Exception e)
            {
            }
            return null;
        }

        #endregion

        private void StartAnimationWindow(object sender, RoutedEventArgs e)
        {
            AnimationWindow window = new AnimationWindow();
            window.Show();
        }
    }
}
