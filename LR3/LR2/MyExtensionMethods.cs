﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace LR3
{
    public static class MyExtensionMethods
    {
        public static bool Contains<T>(this ObservableCollection<T> items, T item, bool flag) where T : IComparable<T>
        {
            if (!flag)
                return items.Contains(item);
            foreach (T i in items)
                if (i.CompareTo(item) == 0)
                    return true;
            return false;
        }

        public static int GetElementIndex<T>(this ObservableCollection<T> items, T item) where T : IComparable<T>
        {
            for (int i = 0; i < items.Count; i++)
                if (items[i].CompareTo(item) == 0)
                    return i;
            return -1;
        }

        public static void SetCollection<T>(this ObservableCollection<T> firstcollection, ObservableCollection<T> secondcollection) where T : IComparable<T>
        {
            firstcollection.Clear();
            foreach (T item in secondcollection)
                firstcollection.Add(item);
        }

        public static void SetCollection<T>(this ObservableCollection<T> firstcollection, IEnumerable<IGrouping<string, T>> groups) where T : IComparable<T>
        {
            if (groups.Count() == 1)
            {
                firstcollection.Clear();
                IGrouping<string, T> group = groups.Single();
                foreach (T item in group)
                    firstcollection.Add(item);
            }
        }

    }


}
