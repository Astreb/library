﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.IO;

namespace LR3
{
    [Serializable]
    public class PublishingHouse : IComparable<PublishingHouse>, INotifyPropertyChanged
    {
        private string name;
        private string city;
        public string Name
        {
            get{ return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }
        public string City
        {
            get { return city; }
            set
            {
                city = value;
                OnPropertyChanged("City");
            }
        }
        public ObservableCollection<Book> Books;

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string par = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(par));
        }

        public PublishingHouse()
        {
            Name = "";
            City = "";
            Books = new ObservableCollection<Book>();
        }

        public PublishingHouse(PublishingHouse pubhouse)
        {
            Name = pubhouse.Name;
            City = pubhouse.City;
            Books = new ObservableCollection<Book>(pubhouse.Books);
        }

        public void SetValue(PublishingHouse pubhouse)
        {
            Name = pubhouse.Name;
            City = pubhouse.City;
        }

        public int CompareTo(PublishingHouse pub_house)
        {
            if (pub_house.Name != Name)
                return -1;
            if (pub_house.City != City)
                return -1;
            return 0;
        }

        public void AddBook(Book book)
        {
            Books.Add(book);
        }

        public void RemoveBook(Book book)
        {
            Books.RemoveAt(Books.IndexOf(book));
        }

        public void Print(StreamWriter stream)
        {
            stream.WriteLine(Name);
            stream.WriteLine(City);
        }

        public void Read(StreamReader stream)
        {
            Name = stream.ReadLine();
            City = stream.ReadLine();
        }
    }
}
