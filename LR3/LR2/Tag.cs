﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.IO;

namespace LR3
{
    [Serializable]
    public class Tag : IComparable<Tag>
    {
        public string tag { get; set; }
        public ObservableCollection<Book> Books { get; set; }

        public int CompareTo(Tag tagg)
        {
            if (tagg.tag != tag)
                return -1;
            return 0;
        }

        public Tag()
        {
            tag = "";
            Books = new ObservableCollection<Book>();
        }

        public Tag(string tag)
        {
            this.tag = tag;
            Books = new ObservableCollection<Book>();
        }
    }
}
