﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyInterface;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using LR3;

namespace LoadFromBackup
{
    public class LoadFromBackup : IPluggin
    {
        public string Name { get; set; }

        public void Process(Library myLibrary)
        {
            string filename = "backup.txt";
                BinaryFormatter formater = new BinaryFormatter();
                using (FileStream fs = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Read))
                {
                    Library lib = (Library)formater.Deserialize(fs);
                    if (lib.Books != null)
                    {
                        foreach (Book i in lib.Books)
                            if (!myLibrary.Books.Contains(i, true))
                                    myLibrary.Books.Add(i);
                    }
                    if (lib.Authors != null)
                    {
                        foreach (Author i in lib.Authors)
                            if (!myLibrary.Authors.Contains(i, true))
                                    myLibrary.Authors.Add(i);
                    }
                    if (lib.PubHouse != null)
                    {
                        foreach (PublishingHouse i in lib.PubHouse)
                            if (!myLibrary.PubHouse.Contains(i, true))
                                    myLibrary.PubHouse.Add(i);
                    }
                    if (lib.Tags != null)
                    {
                        foreach (Tag i in lib.Tags)
                            if (!myLibrary.Tags.Contains(i))
                                    myLibrary.Tags.Add(i);
                    }
                }
        }

        public LoadFromBackup()
        {
            Name = "LoadFromBackup";
        }
    }
}
