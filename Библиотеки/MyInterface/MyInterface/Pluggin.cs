﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LR3;

namespace MyInterface
{
    public interface IPluggin
    {
        string Name { get; set; }
        void Process(Library myLibrary);
    }
}
